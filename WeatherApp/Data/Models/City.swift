//
//  City.swift
//  WeatherFramwork
//
//  Created by sami hazel on 28/06/2023.
//

import Foundation

public struct City: Codable {
    
    enum CodingKeys: String, CodingKey {
        case country
        case name
        case lat
        case lng
    }
    
    var country: String?
    var name: String?
    var lat: String?
    var lng: String?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        country = try container.decodeIfPresent(String.self, forKey: .country)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        lat = try container.decodeIfPresent(String.self, forKey: .lat)
        lng = try container.decodeIfPresent(String.self, forKey: .lng)
    }
}
